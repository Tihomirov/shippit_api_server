class ApiController < ApplicationController
  protect_from_forgery except: :xml

  def xml
    @coords = random_position

    respond_to do |format|
      if shipment_request?
        format.xml { render :create_shipment }
      else
        format.xml { render :track_shipment }
      end
    end
  end

  def shipment_request?
    request_body = request.body.as_json
    request_type = Hash.from_xml(request_body.join.delete("\n")).keys[0]
    true if request_type == 'AramexShipmentRequest'
  end

  def random_position
    geo_index = rand(2)
    coords = {
      lat: [42.69481410708031, -33.87000407061607, 41.9828119],
      long: [23.31658863809139, 151.16589398227654, -87.7161638]
    }

    { lat: coords[:lat][geo_index], long: coords[:long][geo_index] }
  end
end
