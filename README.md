# README

This is a static responder server for my *https://gitlab.com/Tihomirov/shippit* app

### Ruby version

```
2.3.5
```

### Configuration

```
bundle install
```

#### Run It

```
rails server -port 3300
```

Remember to add the host of the server to the main shippit's app .env.development
